import random
import pygame

WIDTH = 1000
HEIGHT = 600
RESOLUTION = 20
FRAMERATE = 15

def main():
    random.seed()
    pygame.display.init()
    clock = pygame.time.Clock()
    life = Cells(WIDTH, HEIGHT, RESOLUTION)
    is_running = True
    while is_running:
        clock.tick(FRAMERATE)
        life.draw()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_running = False

class Cells:
    def __init__(self, display_width, display_height, cells_resolution):
        self.resolution = cells_resolution
        self.columns = display_width // cells_resolution
        self.rows = display_height // cells_resolution
        self.list_alive = [[0] * self.rows for i in range(self.columns)]
        self.screen = pygame.display.set_mode([display_width, display_height])

        for i in range(self.columns):
            for j in range(self.rows):
                rand = random.randint(0, 1)
                self.list_alive[i][j] = rand

    def check_generation(self):
        list_nextgen = [[0] * self.rows for i in range(self.columns)]

        for i in range(self.columns):
            for j in range(self.rows):
                state = self.list_alive[i][j]
                neighbors = self.count_neighbors(i, j)
                if state == 0 and neighbors == 3:
                    list_nextgen[i][j] = 1
                elif (neighbors < 2 or neighbors > 3) and state == 1:
                    list_nextgen[i][j] = 0
                else:
                    list_nextgen[i][j] = state

        self.list_alive = list_nextgen

    def count_neighbors(self, posx, posy):
        summary = 0
        for i in range(-1, 2):
            for j in range(-1, 2):
                count_col = (i + posx + self.columns) % self.columns
                count_row = (j + posy + self.rows) % self.rows
                summary += self.list_alive[count_col][count_row]
        summary -= self.list_alive[posx][posy]
        return summary

    def draw(self):
        black = (0, 0, 0)
        white = (255, 255, 255)
        for i in range(self.columns):
            for j in range(self.rows):
                cellx = i * self.resolution
                celly = j * self.resolution
                if self.list_alive[i][j] == 1:
                    pygame.draw.rect(self.screen, white, [cellx, celly
                                                          , self.resolution-1
                                                          , self.resolution-1])
                else:
                    pygame.draw.rect(self.screen, black, [cellx, celly
                                                          , self.resolution
                                                          , self.resolution])

        self.check_generation()

main()
