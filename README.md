# pygameoflife

John Conway's game of life created with Python, the pygame module and OOP.

![](gameoflife-example.gif)